package testtask.data;

import com.fasterxml.jackson.annotation.*;
import io.swagger.annotations.*;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonRootName("RESPONSE")
@Entity
@Table(name = "APPLICATIONS")
@ApiModel(description = "Details about application")
public class Dto
{
    @Id
    @Column(name = "APPLICATION_ID")
    @JsonProperty("APPLICATION_ID")
    @ApiModelProperty(notes = "Application id")
    private Long id;

    @Column(name = "CONTACT_ID")
    @JsonProperty("CONTACT_ID")
    @ApiModelProperty(notes = "Contact id")
    private Long contactId;

    @Column(name = "DT_CREATED")
    @JsonProperty("DT_CREATED")
    @ApiModelProperty(notes = "Creation date")
    private Date createdAt;

    @JsonProperty("PRODUCT_NAME")
    @ApiModelProperty(notes = "Product name")
    private String productName;
}
