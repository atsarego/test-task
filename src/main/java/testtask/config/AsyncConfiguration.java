package testtask.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.*;

import java.util.concurrent.*;

@Configuration
@EnableAsync
public class AsyncConfiguration implements AsyncConfigurer
{
    @Override
    public Executor getAsyncExecutor()
    {
        return Executors.newFixedThreadPool(10 /*what is a best number?*/);
    }
}
