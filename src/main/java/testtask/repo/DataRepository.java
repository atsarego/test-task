package testtask.repo;

import org.springframework.data.jpa.repository.*;
import testtask.data.Dto;

public interface DataRepository extends JpaRepository<Dto, Long>
{
    @Query(
            value = "select * from applications where contact_id = ?1 order by dt_created desc fetch first 1 row only",
            nativeQuery = true
    )
    Dto findLastByContactId(Long contactId);
}
