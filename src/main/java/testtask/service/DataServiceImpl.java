package testtask.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import testtask.repo.DataRepository;
import testtask.data.Dto;

@Service
public class DataServiceImpl implements DataService
{
    @Autowired
    private DataRepository dataRepository;

    @Override
    public Dto getLastApplicationByContact(Long contactId)
    {
        return dataRepository.findLastByContactId(contactId);
    }
}
