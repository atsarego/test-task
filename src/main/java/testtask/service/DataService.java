package testtask.service;

import testtask.data.Dto;

public interface DataService
{
    Dto getLastApplicationByContact(Long contactId);
}
