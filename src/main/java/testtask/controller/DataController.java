package testtask.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import testtask.data.Dto;
import testtask.service.DataService;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api")
public class DataController
{
    private DataService dataService;

    @Autowired
    public DataController(DataService dataService)
    {
        this.dataService = dataService;
    }

    @GetMapping(
            path = "/contacts/{id}",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}
    )
    @ResponseBody
    @ApiOperation(value = "Get last application info by contact id", response = Dto.class)
    public CompletableFuture<Dto> data(
            @ApiParam(value = "contact id", required = true) @PathVariable Long id
    )
    {
        return CompletableFuture.supplyAsync(() -> dataService.getLastApplicationByContact(id));
    }
}
