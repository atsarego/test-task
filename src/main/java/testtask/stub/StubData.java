package testtask.stub;

import com.google.common.collect.ImmutableSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.*;
import org.springframework.stereotype.Component;
import testtask.repo.DataRepository;
import testtask.data.Dto;

import java.time.Instant;
import java.util.Date;

@Component
public class StubData implements ApplicationRunner
{
    private static final Long CONTACT_ID_1 = 1L;
    private static final Long CONTACT_ID_2 = 2L;
    private static final Long APPLICATION_ID_1 = 11L;
    private static final Long APPLICATION_ID_2 = 12L;
    private static final Long APPLICATION_ID_3 = 13L;
    private static final Long APPLICATION_ID_4 = 14L;
    private static final Date DATE_1 = Date.from(Instant.parse("2019-11-01T00:00:00.00Z"));
    private static final Date DATE_2 = Date.from(Instant.parse("2019-11-02T00:00:00.00Z"));
    private static final Date DATE_3 = Date.from(Instant.parse("2019-11-03T00:00:00.00Z"));
    private static final Date DATE_4 = Date.from(Instant.parse("2019-11-04T00:00:00.00Z"));
    private static final String PRODUCT_1 = "Product 1";
    private static final String PRODUCT_2 = "Product 2";
    private static final String PRODUCT_3 = "Product 3";
    private static final String PRODUCT_4 = "Product 4";

    private final DataRepository dataRepository;

    @Autowired
    public StubData(DataRepository dataRepository)
    {
        this.dataRepository = dataRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception
    {
        dataRepository.saveAll(ImmutableSet.of(
                new Dto(APPLICATION_ID_1, CONTACT_ID_1, DATE_1, PRODUCT_1),
                new Dto(APPLICATION_ID_2, CONTACT_ID_1, DATE_2, PRODUCT_2),
                new Dto(APPLICATION_ID_3, CONTACT_ID_2, DATE_3, PRODUCT_3),
                new Dto(APPLICATION_ID_4, CONTACT_ID_2, DATE_4, PRODUCT_4)
        ));
    }
}
