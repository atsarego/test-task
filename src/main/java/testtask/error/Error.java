package testtask.error;

import lombok.Data;

@Data
public class Error
{
    private final String message;
}
