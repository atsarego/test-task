package testtask.error;

import org.hibernate.JDBCException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice
public class JdbcExceptionHandler
{
    @ExceptionHandler(JDBCException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Error handleJdbcException(JDBCException exception)
    {
        return new Error(exception.getMessage());
    }
}
