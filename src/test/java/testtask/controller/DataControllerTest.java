package testtask.controller;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.*;
import testtask.controller.DataController;
import testtask.data.Dto;
import testtask.service.DataService;

import java.time.Instant;
import java.util.Date;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.ResultMatcher.matchAll;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(DataController.class)
public class DataControllerTest
{
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DataService dataService;

    @BeforeEach
    public void setUp()
    {
        given(dataService.getLastApplicationByContact(1L)).willReturn(
                new Dto(0L, 1L, Date.from(Instant.ofEpochMilli(1000)), "product name")
        );
    }

    @Test
    public void test_json() throws Exception
    {
        MvcResult mvcResult = mockMvc.perform(get("/api/contacts/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(request().asyncStarted())
                .andReturn();

        mockMvc.perform(asyncDispatch(mvcResult))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$['APPLICATION_ID']", is(0)))
                .andExpect(jsonPath("$['CONTACT_ID']", is(1)))
                //TODO: how to verify date?
                //.andExpect(jsonPath("$['DT_CREATED']", is()))
                .andExpect(jsonPath("$['PRODUCT_NAME']", is("product name")));
    }

    @Test
    public void test_xml() throws Exception
    {
        MvcResult mvcResult = mockMvc.perform(get("/api/contacts/1").accept(MediaType.APPLICATION_XML))
                .andExpect(request().asyncStarted())
                .andReturn();

        mockMvc.perform(asyncDispatch(mvcResult))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_XML))
                .andExpect(matchAll(
                        xpath("/RESPONSE").exists(),
                        xpath("/RESPONSE/APPLICATION_ID").number(0d),
                        xpath("/RESPONSE/CONTACT_ID").number(1d),
                        //TODO: how to verify date?
                        //xpath("/RESPONSE/DT_CREATED").string()
                        xpath("/RESPONSE/PRODUCT_NAME").string("product name")
                ));
    }
}
