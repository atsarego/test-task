package testtask.repo;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.*;
import testtask.data.Dto;

import java.time.Instant;
import java.util.Date;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@DataJpaTest
public class DtoRepositoryTest
{
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private DataRepository dataRepository;

    @BeforeEach
    public void setUp()
    {
        entityManager.persist(new Dto(0L, 0L, Date.from(Instant.ofEpochMilli(1000)), "product name 1"));
        entityManager.persist(new Dto(1L, 0L, Date.from(Instant.ofEpochMilli(2000)), "product name 2"));
        entityManager.flush();
    }

    @Test
    public void test()
    {
        assertThat(dataRepository.findLastByContactId(0L).getProductName(), equalTo("product name 2"));
    }
}
